import { Telegraf } from "telegraf";
import dotenv from "dotenv";
import express from "express";

const app = express();

dotenv.config();

const bot = new Telegraf(process.env.TOKEN);

const groups = [
  [
    {
      url: "https://t.me/+bgbiUst6X45kNDdh",
      text: "Grupo General",
    },
  ],
  [
    {
      url: "https://t.me/+iJ1JMf2GiXE0ZDUx",
      text: "CABA",
    },
  ],
  [
    {
      url: "https://t.me/+N_Q4ulW0gRI5MjRh",
      text: "Buenos Aires - Zona Oeste",
    },
  ],
  [
    {
      url: "https://t.me/+gkPHeFQSmvs1Nzk5",
      text: "Buenos Aires - Zona Norte",
    },
  ],
  [
    {
      url: "https://t.me/+gkPHeFQSmvs1Nzk5",
      text: "Buenos Aires - Zona Sur",
    },
  ],
  [
    {
      url: "https://t.me/+kLxcfv1m1MEwMTAx",
      text: "La Plata",
    },
  ],
  [
    {
      url: "https://t.me/+MjZh-vvCvaMyYzBh",
      text: "Provincia de Córdoba",
    },
  ],
  [
    {
      url: "https://t.me/+kMSrlcPFKddkNzM5",
      text: "Provincia de Santa Fe",
    },
  ],
  [
    {
      url: "https://t.me/+4Om7zdD_o94wMjgx",
      text: "Provincia de Mendoza",
    },
  ],
  [
    {
      url: "https://t.me/+d9V5YyAgQ4M4MjZh",
      text: "Uruguay",
    },
  ],
];

const rules = `REGLAS DEL GRUPO

1) para hablar o enviarse contenido con alguien por privado se necesita consentimiento explícito. La no respuesta es considerada no.

2) no se pueden ofrecer/vender ni hacer apología de productos o servicios sexuales, drogas, productos ilegales.

3) no se pueden enviar al grupo fotos explicitas. Si: sugerentes, eróticas, sensuales. Si alguien quiere ver mas, sigue la regla 1). Así como no se permiten imágenes de personas menores de edad ni terceras personas no pertenecientes al grupo.

4) no tengo responsabilidad alguna por lo que ocurra en los encuentros presenciales que surjan de este grupo -individuales y/o grupales- nos basamos en la responsabilidad sexoafectiva como lema para encontrarnos con otr@s.

5) es un grupo para divertirse, hacer amig@s, conocer gente y hablar 24/7. Sientanse bienvenid@s de hacer lo que quieran siguiendo estas reglas simples.`;

try {
  bot.start((ctx) => {
    let id = ctx.update.message.chat.id;
    let text = rules;
    let parse_mode = "markdown";
    let reply_markup = {
      inline_keyboard: [
        [
          {
            text: "Aceptar",
            callback_data: "aceptar",
          },
        ],
      ],
    };

    bot.telegram.sendMessage(id, text, { reply_markup, parse_mode });
  });

  bot.action("aceptar", (ctx) => {
    let text = `Buenísimo! Ya podés ingresar al grupo, también hay varios grupos por zona, provincia o país! Suate al que quieras!`;
    let id = ctx.update.callback_query.message.chat.id;
    let message_id = ctx.update.callback_query.message.message_id;
    ctx.answerCbQuery();
    ctx.deleteMessage(message_id);
    bot.telegram.sendMessage(id, text, {
      reply_markup: {
        inline_keyboard: [
          [
            {
              text: "Grupo general",
              url: "https://t.me/+bgbiUst6X45kNDdh",
            },
          ],
          [
            {
              text: "Ver grupos zonales",
              callback_data: "otros",
            },
          ],
        ],
      },
      parse_mode: "markdown",
    });
  });

  bot.action("otros", (ctx) => {
    let text = `Estos son los grupos zonales. Recordá que las reglas se aplican a cualquier gurpo al que te sumes!`;
    let id = ctx.update.callback_query.message.chat.id;
    let message_id = ctx.update.callback_query.message.message_id;
    ctx.answerCbQuery();
    ctx.deleteMessage(message_id);
    bot.telegram.sendMessage(id, text, {
      reply_markup: { inline_keyboard: groups },
      parse_mode: "Markdown",
    });
  });

  const { PORT = 3000 } = process.env;
  app.listen(PORT, async () => {
    await bot.launch();
    console.log(`bot corriendo en el puerto ${PORT}`);
  });
} catch (error) {
  console.log(error);
}
